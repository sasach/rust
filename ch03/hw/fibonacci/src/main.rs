use std::io;
fn main() {
    println!("Enter the value of n:");
    let mut n = String::new();
    io::stdin().read_line(&mut n).expect("Failed to read line.");
    let n: u32 = n.trim()
        .parse()
        .expect("Please type a number.");
    let ans = fibonacci(n);
    /*
    let mut a = 0;
    let mut b = 1;
    let mut c = 0;
    for _i in 2..n {
        c = a+b;
        a = b;
        b = c;
    }
    println!("nth Fibonacci number: {}", c);   
    */
    println!("nth Fibonacci number is: {}", ans);
}
fn fibonacci(n: u32) -> u32 {
    if n == 2 {
        return 1;
    }
    else if n == 1 {
        return 0;
    }
    else {
        return fibonacci(n-1) + fibonacci(n-2);
    }
}