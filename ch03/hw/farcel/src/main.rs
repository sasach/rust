use std::io;
fn main() {
    println!("Enter temperature in Fahrenheit:");
    let mut far = String::new();
    io::stdin().read_line(&mut far).expect("Failed to read line.");
    let far: f64 = far.trim()
        .parse()
        .expect("Please type a number.");
    let cel: f64 = (5.0*far)/9.0 - 32.0;
    println!("Celsius: {}", cel);   
}
