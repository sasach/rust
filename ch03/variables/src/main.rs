fn main() {
    let mut x = 5;
    println!("The value of x is: {}", x);
    x=6;
    println!("The value of x is: {}", x);
    let tup: (i32, f64, u8) = (500, 6.4, 1);
    let (x, y, z) = tup;
    println!("The value of y is: {}", y);
    let a = [1, 2, 3, 4, 5];
    let a: [i32; 5] = [1, 2, 3, 4, 5];
    let a = [10, 20, 30, 40, 50];
    for element in a.iter() {
        println!("{}", element);   
    }  
}
